#!/bin/bash

FLOW=(Farneback tvl1 ldof epicflow flowfield pwcnet Farneback_NA tvl1_NA ldof_NA epicflow_NA flowfield_NA pwcnet_NA )

BDD=( CK+ CASIA SNAP )

descriptor="LMP"
#descriptor="HOF"
#descriptor="HOOF"

path="PATH_TO_THE_GIT_FOLDER/SVM_and_handcrafted/$descriptor/"

rm results.csv
rm aaa.csv
rm bbb.csv

for B in ${BDD[@]}; do

	echo $B >> "results.csv"

	for F in ${FLOW[@]}; do

		echo $F >> "results.csv"

		for P in {1..10}; do

			./svm-train -t 0 $path$descriptor"_"$B"_"$F"_proto_"$P"_train.csv" "proto.model"

			./svm-predict $path$descriptor"_"$B"_"$F"_proto_"$P"_test.csv" "proto.model" "proto.output" > bbb.csv

			#cat bbb.csv >> results.csv

			cat bbb.csv | grep "Accuracy = " | sed "s/^.*=//" | cut -c2-3 >> aaa.csv
			cat bbb.csv | grep "Accuracy = " | sed "s/^.*=//" | cut -c2-3 >> results.csv

		done

		awk 'BEGIN{ somme=0 } { somme=somme+$0 } END{ print somme/10 }' aaa.csv >> results.csv
		echo "" >> results.csv

		rm aaa.csv

	done

done
