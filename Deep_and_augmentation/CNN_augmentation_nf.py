#Load Packages
import numpy as np
import tensorflow as tf
import random as rn
import sys

np.random.seed(42)
rn.seed(12345)

session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

import keras
from keras import backend as K

tf.set_random_seed(1234)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

#import keras
#import keras.utils
from keras.models import Sequential, Model
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import SimpleRNN
from keras.layers import Activation, Flatten, Dropout
from keras.layers import *
from keras.utils import to_categorical
from keras import optimizers
from sklearn import preprocessing
from sklearn import svm, datasets
from sklearn.exceptions import DataConversionWarning
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import confusion_matrix
import pandas as pd
import argparse

warnings.filterwarnings(action='ignore', category=DataConversionWarning)

#parser = argparse.ArgumentParser()
#parser.add_argument("--verbosity", help="increase output verbosity")
#args = parser.parse_args()
#if args.verbosity:
#	print("verbosity turned on")

# XXXXXXXXXXXXX   data preparation   XXXXXXXXXXXXXXXXX

opticalflow = str(sys.argv[2]).split(',')
print(opticalflow)

BDD = sys.argv[1]
FLOW = opticalflow[0]
FLOWS = []
for i in range(1,len(opticalflow)):
	if(opticalflow[i]!=''):
		FLOWS.append(opticalflow[i])

print(FLOWS)

num_classes = 7
dims = 50
channels = 3
epochs = 10
batch_size = 8

def augmentation(FLOW, combined_data_train_x, combined_data_train_y):
	data_train_aug = pd.read_csv("MODELS_CNN/CNN_" + BDD + "_" + FLOW + "_proto_" + str(i+1) + "_train.csv",sep=';', header=None)
	data_train_aug = np.array(data_train_aug, dtype=float)
	data_train_y_aug = [col[0] for col in data_train_aug]
	data_train_y_aug = np.array(data_train_y_aug,dtype=int)
	data_train_x_aug = np.delete(data_train_aug,0,1)
	combined_data_train_x = np.append(combined_data_train_x, data_train_x_aug)
	combined_data_train_y = np.append(combined_data_train_y, data_train_y_aug)
	return combined_data_train_x, combined_data_train_y


fold = 0
cvscores = []
for i in range(0,10):

	data_train = pd.read_csv("MODELS_CNN/CNN_" + BDD + "_" + FLOW + "_proto_" + str(i+1) + "_train.csv",sep=';', header=None)
	data_train = np.array(data_train, dtype=float)

	data_train_y = [col[0] for col in data_train]
	data_train_y = np.array(data_train_y,dtype=int)

	data_train_x = np.delete(data_train,0,1)

	#--------------------------------------------

	combined_data_train_x = data_train_x
	combined_data_train_y = data_train_y

	for f in FLOWS:
		combined_data_train_x, combined_data_train_y = augmentation(f, combined_data_train_x, combined_data_train_y)

	cat_data_y_train = to_categorical(combined_data_train_y)
	augmented_data_train_x = combined_data_train_x.reshape(combined_data_train_y.shape[0],channels,dims,dims)

	#--------------------------------------------

	data_test = pd.read_csv("MODELS_CNN/CNN_" + BDD + "_" + FLOW + "_proto_" + str(i+1) + "_test.csv",sep=';', header=None)
	data_test = np.array(data_test, dtype=float)

	data_test_y = [col[0] for col in data_test]
	data_test_y = np.array(data_test_y,dtype=int)

	data_test_x = np.delete(data_test,0,1)
	data_test_x = data_test_x.reshape(data_test_y.shape[0],channels,dims,dims)

	cat_data_y_test = to_categorical(data_test_y)

	fold = fold + 1

	x = Input(shape=(channels, dims, dims))

	tower_1 = Conv2D(8, (5,5), padding='same', activation='relu', input_shape=(channels,dims,dims), data_format='channels_first',kernel_initializer=keras.initializers.RandomNormal(seed=42), bias_initializer=keras.initializers.Constant(value=0.1))(x)
	tower_1 = BatchNormalization()(tower_1)
	tower_1 = MaxPooling2D(pool_size=(2,2), data_format='channels_first', padding='same')(tower_1)
	tower_1 = Conv2D(16,(3,3), padding='same', activation='relu', data_format='channels_first',kernel_initializer=keras.initializers.RandomNormal(seed=42), bias_initializer=keras.initializers.Constant(value=0.1))(tower_1)
	tower_1 = BatchNormalization()(tower_1)
	tower_1 = MaxPooling2D(pool_size=(2,2), data_format='channels_first', padding='same')(tower_1)
	tower_1 = Conv2D(32,(3,3), padding='same', activation='relu', data_format='channels_first',kernel_initializer=keras.initializers.RandomNormal(seed=42), bias_initializer=keras.initializers.Constant(value=0.1))(tower_1)
	tower_1 = BatchNormalization()(tower_1)
	tower_1 = MaxPooling2D(pool_size=(2,2), data_format='channels_first', padding='same')(tower_1)

	merged = Flatten()(tower_1)

	out = Dense(256, activation='relu',kernel_initializer=keras.initializers.RandomNormal(seed=42), bias_initializer=keras.initializers.Constant(value=0.1))(merged)
	out = Dropout(0.4)(out)
	out = Dense(num_classes, activation='softmax')(out)

	conv_model = Model(x, out)

	model = Sequential()
	model.add(conv_model)

	adam = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
	model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

#	if args.verbosity:
#		model.summary() # print model scheme (layers and parameters)

	# XXXXXXXXXXXXX   run the model   XXXXXXXXXXXXXXXXX

#	verbose = 0
#	if args.verbosity:
#		verbose = 2

	history = model.fit(augmented_data_train_x, cat_data_y_train, batch_size=batch_size, shuffle=False, epochs=epochs, verbose=0)
	score = model.evaluate(data_test_x, cat_data_y_test, verbose=0)
	preds = model.predict_classes(data_test_x, verbose=0)

	#print("Fold %i : %.2f%%" % (fold, score[1]*100))
	cvscores.append(score[1]*100)

# FINAL RUN ACCURACY
print("ACC= %.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))
