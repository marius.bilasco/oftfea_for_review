#Load Packages
import numpy as np
import keras
import keras.utils
from keras.models import Sequential, Model
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import SimpleRNN
from keras.layers import Activation, Flatten, Dropout
from keras.layers import *
from keras.utils import to_categorical
from keras import optimizers
from sklearn import preprocessing
from sklearn import svm, datasets
from sklearn.exceptions import DataConversionWarning
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import confusion_matrix
import pandas as pd
import argparse

warnings.filterwarnings(action='ignore', category=DataConversionWarning)

parser = argparse.ArgumentParser()
parser.add_argument("--verbosity", help="increase output verbosity")
args = parser.parse_args()
if args.verbosity:
	print("verbosity turned on")

# XXXXXXXXXXXXX   data preparation   XXXXXXXXXXXXXXXXX

#BDD = "CASIA"
BDD = "CK+"
#BDD = "SNAP"

#FLOW = "epicflow"
FLOW = "Farneback"
#FLOW = "flowfield"
#FLOW = "ldof"
#FLOW = "tvl1"

num_classes = 7
dims = 50
channels = 3
epochs = 10
timestep = 9
batch_size = 8

configuration = 0
cvscores = []
for i in range(0,10):

	data_train = pd.read_csv("MODELS_RNN/RNN_" + BDD + "_" + FLOW + "_proto_" + str(i+1) + "_train.csv",sep=';', header=None)
	data_train = np.array(data_train, dtype=float)

	data_train_y = [col[0] for col in data_train]
	data_train_y = np.array(data_train_y,dtype=int)

	data_train_x = np.delete(data_train,0,1)
	data_train_x = data_train_x.reshape(data_train_y.shape[0],timestep,channels,dims,dims)

	data_test = pd.read_csv("MODELS_RNN/RNN_" + BDD + "_" + FLOW + "_proto_" + str(i+1) + "_test.csv",sep=';', header=None)
	data_test = np.array(data_test, dtype=float)

	data_test_y = [col[0] for col in data_test]
	data_test_y = np.array(data_test_y,dtype=int)

	data_test_x = np.delete(data_test,0,1)
	data_test_x = data_test_x.reshape(data_test_y.shape[0],timestep,channels,dims,dims)

	configuration = configuration + 1

	model = Sequential()

	model.add(Conv3D(8,(1,5,5), padding='same', activation='relu', input_shape=(timestep,channels,dims,dims), data_format='channels_first'))
	model.add(BatchNormalization())
	model.add(MaxPooling3D(pool_size=(1,2,2), data_format='channels_first', padding='same'))
	model.add(Conv3D(16,(1,3,3), padding='same', activation='relu', data_format='channels_first'))
	model.add(BatchNormalization())
	model.add(MaxPooling3D(pool_size=(1,2,2), data_format='channels_first', padding='same'))
	model.add(Conv3D(32,(1,3,3), padding='same', activation='relu', data_format='channels_first'))
	model.add(BatchNormalization())
	model.add(MaxPooling3D(pool_size=(1,2,2), data_format='channels_first', padding='same'))

	model.add(ConvLSTM2D(32, (3,3), padding='same', activation='relu', return_sequences=False, data_format='channels_first'))

	model.add(Flatten())

	model.add(Dense(256, activation='relu'))
	model.add(Dropout(0.4))
	model.add(Dense(num_classes, activation='softmax'))

	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

	if args.verbosity:
		model.summary() # print model scheme (layers and parameters)

	# XXXXXXXXXXXXX   run the model   XXXXXXXXXXXXXXXXX

	cat_data_y_train = to_categorical(data_train_y)
	cat_data_y_test = to_categorical(data_test_y)

	verbose = 0
	if args.verbosity:
		verbose = 2

	history = model.fit(data_train_x, cat_data_y_train, batch_size=batch_size, epochs=epochs, verbose=verbose)
	score = model.evaluate(data_test_x, cat_data_y_test, verbose=verbose)
	preds = model.predict_classes(data_test_x, verbose=verbose)

	print("Configuration %i : %.2f%%" % (configuration, score[1]*100))
	cvscores.append(score[1]*100)

# FINAL RUN ACCURACY
print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))
