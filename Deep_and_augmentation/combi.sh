#!/bin/bash

rm results.txt

BDD=( CK+ CASIA SNAP )

for B in ${BDD[@]};
do
    echo $B >> results.txt
    for line in $(cat combinaison.txt);
    do
        echo $line >> results.txt
        python3 CNN_augmentation_nf.py $B $line > aaa.txt
        cat aaa.txt | grep "ACC= " | sed "s/^.*=//" | cut -c2-5 >> results.txt
    done
done
