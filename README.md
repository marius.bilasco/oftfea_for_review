This repository contains the scripts to generate results for the paper "Performance Evaluation of Optical Flow Techniques for Facial Expression Analysis", submitted to IJCV.

This deposit contains three files :
- *Configurations_per_datasets*
- *SVM_and_handcrafted*
- *Deep_and_augmentation*

== **Configurations_per_datasets**

In order to uniformly evaluate all optical flow approaches, we have randomly generated ten learning configurations. Performance is calculated on exactly the same ten 60-40 train/test validation configurations, in order to ensure uniform evaluation of all optical flow approaches

The *Configurations_per_datasets* folder contains the sequences that we have kept for each dataset after the standardization of the data. In each file, the data format is as follows: subject, sequence, label and 10 supplementary columns indicating whether the sample belongs to the train (1) or the test (0) part of each configuration.

== **SVM_and_handcrafted**

This folder contains three sub-folders containing the different learning models generated for the different approaches used (HOF, HOOF and LMP).
The cmd_learning.sh script allows you to run these models to reproduce the results presented in the article. To do this, simply drop the script in the directory where libsvm is located. Then fill in the script the path to the learning models. The results will be saved in a file named results.csv in the current libsvm directory.
By default, the descriptor used is LMP. If you want to run experiments using HOF and HOOF, please uncomment the corresponding lines in *cmd_learning.sh* .

== **Deep_and_augmentation**

To run this part, you need to install TensorFlow, Keras, Sklearn, Pandas.

This folder contains the script to run the CNN (*CNN_channel_flow_proto.py*) and RNN (*RNN_channel_flow_proto.py*) experiments reported in the paper. The learning models are located in the *MODELS_CNN* and *MODELS_RNN* folders depending on the architecture used and the format of the required data (TIM2 or TIM10). All 10 train/test configurations are tested and mean and standard deviation are reported.

CNN experiments can be run using all the optical flows extracted between the Neutral frame and the Apex frames (TIM2) on all sequences of each dataset. Please uncomment the desired lines in *CNN_channel_flow_proto.py*.

RNN experiments are run using all the optical flows extracted between 10 selected frames (TIM10) from all sequences of each dataset. 

Due to the important filesize of all RNN models, this GIT instance contains only the CK+ TIM10 configuration.

More data can be downloaded from this [address](https://nextcloud.univ-lille.fr/index.php/s/CBpWNkGPRe4cqkB) in order to test all datasets. Download the desired configuration (BDD+OpticalFLow.zip), unzip it and place the extracted files in the *MODELS_RNN* folder, then uncomment the desired lines in *RNN_channel_flow_proto.py*.

This folder also contains the CNN_augmentation.nf.py script to reproduce the results presented in the data augmentation evaluation.

The combi.sh script automatically reproduces all combinations of analyzed optical flows.
